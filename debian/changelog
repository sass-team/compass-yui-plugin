compass-yui-plugin (0~20100724-4) unstable; urgency=medium

  * Fix use package section web.
  * Update package relations:
    + Depend on recent ruby-sass (not ruby-compass).
    + Stop recommend ruby-compass.
    + Build-depend on licensecheck (not devscripts).
  * Modernize Vcs-* fields:
    + Use https protocol.
    + Use anonscm host.
    + Use git subdir (not cgit).
    + Add .git suffix for Vcs-Git URL.
  * Declare compliance with Debian Policy 3.9.8.
  * Bump debhelper compatibility level to 9.
  * Update copyright info:
    + Use License-Grant and License-Reference fields.
      Thanks to Ben Finney.
    + Extend coverage of Debian packaging.
  * Drop dpkg-source local-options hint: Declared options are default
    since dpkg-source 1.16.1.
  * Add lintian override regarding license in License-Reference field.
    See bug#786450.
  * Add git-buildpage config:
    + Use pristine-tar.
    + Sign tags.
    + Filter any .git* file.
  * Update watch file:
    + Bump to version 4.
    + Add usage comment.
    + Use github pattern from documentation.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 23 Jan 2017 16:06:49 +0100

compass-yui-plugin (0~20100724-3) unstable; urgency=medium

  * Add README.source emphasizing control.in file as *not* a
    show-stopper for contributions, referring to CDBS+git-buildpackage
    wiki page for details.
  * Update Maintainer, Uploaders and Vcs-* fields: Packaging git moved
    to pkg-sass area of Alioth.
  * Bump standards-version to 3.9.5.
  * Modernize watch file to use github directly.
  * Extend coverage for myself, and relicense packaging as GPL-3+.
  * Bump debhelper compatibility level to 8: Supported even on oldstable
    now.
  * Relax to build-depend unversioned on cdbs, debhelper and devscripts:
    Needed versions satisfied even on oldstable.

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 01 May 2014 15:38:48 +0200

compass-yui-plugin (0~20100724-2) unstable; urgency=low

  * Update package relations:
    + Depend on ruby-compass (not older libcompass-ruby).
    + Fallback-depend on ruby-sass, and recommend ruby-compass as well:
      recent Sass is shipped separately from Haml and supports Compass
      modules directly.
  * Bump policy compliance to standards-version 3.9.2.
  * Update copyright file:
    + Update format to draft 174 of DEP5.
    + Consistently quote licenses in License comments.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 04 Jul 2011 02:04:24 +0200

compass-yui-plugin (0~20100724-1) unstable; urgency=low

  * Initial release.
    Closes: #611869.

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 03 Feb 2011 07:12:58 +0100
